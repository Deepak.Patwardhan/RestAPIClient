{
    "frequency": "DAILY",
    "body": {
        "COMMODITIES.SPOT.SPOT_GOLD": {
            "x": [
                20170109,
                20170110,
                20170111,
                20170112,
                20170113
            ],
            "c": [
                1178.5,
                1189.5,
                1178.55,
                1205.05,
                1190.35
            ],
            "type": "SERIES"
        }
    },
    "status": "OK"
}