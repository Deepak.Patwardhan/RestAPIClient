﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTfulAPIConsume
{
    [TestFixture]
    public class JsonParserTests
    {
        [Test]
        public void ParseSpotDailyData()
        {
            string fileName = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Data\SPOT.GOLD.Daily.txt");
            var spotData = JsonParser.parseJsonFromFile(fileName);
            printData(spotData);
        }

        [Test]
        public void ParseSpotHourlyData()
        {
            string fileName = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Data\EUR.USD.Hourly.txt");
            var spotData = JsonParser.parseJsonFromFile(fileName);
            printData(spotData);
        }

        [Test]
        public void ParseComoditiesWeeklyData()
        {
            string fileName = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Data\Commodities.Weekly.txt");
            var spotData = JsonParser.parseJsonFromFile(fileName);
            printData(spotData);
        }

        [Test]
        public void ParseSpotOLHCData()
        {
            string fileName = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Data\EUR.USD.OLHC.txt");
            var spotData = JsonParser.parseJsonFromFile(fileName);
            printData(spotData);
        }
        

        public void printData(ChartingData result)
        {
            Console.WriteLine($"Frequency: {result.frequency}");
            Console.WriteLine();

            foreach (var item in result.data)
            {
                Console.WriteLine($"Tag: {item.Key}");

                var spotData = item.Value;

                Console.WriteLine($"Date/Time(s)={string.Join(", ", spotData.Dates)}");
                if (spotData.OpenRates != null)
                    Console.WriteLine($"OpenPrices={string.Join(", ", spotData.OpenRates)}");
                if (spotData.LowRates != null)
                    Console.WriteLine($"LowPrices={string.Join(", ", spotData.LowRates)}");
                if (spotData.HighRates != null)
                    Console.WriteLine($"HighPrices={string.Join(", ", spotData.HighRates)}");
                Console.WriteLine($"ClosePrices={string.Join(", ", spotData.CloseRates)}");
                Console.WriteLine($"Type={spotData.SpotType}");
                Console.WriteLine();
            }
            Console.WriteLine($"Status: {result.status}");


        }
    }
}

