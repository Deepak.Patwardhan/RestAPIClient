﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RESTfulAPIConsume.Constants;
using RESTfulAPIConsume.RequestHandlers;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Windows.Forms;

namespace RESTfulAPIConsume
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Fetching the list of RestSharp releases and their publish dates.");
        //    Console.WriteLine();

        //    //These are the five ways to consume RESTful APIs described in the blog post
        //    IRequestHandler httpWebRequestHandler = new HttpWebRequestHandler();
        //    IRequestHandler webClientRequestHandler = new WebClientRequestHandler();
        //    IRequestHandler httpClientRequestHandler = new HttpClientRequestHandler();
        //    IRequestHandler restSharpRequestHandler = new RestSharpRequestHandler();
        //    IRequestHandler serviceStackRequestHandler = new ServiceStackRequestHandler();

        //    //Currently HttpWebRequest is used to get the RestSharp releases
        //    //Replace the httpWebRequestHandler variable with one of the above to test out different libraries
        //    //Results should be the same
        //    var releases = GetReleases(httpWebRequestHandler);

        //    //List out the retreived releases
        //    foreach (JObject release in releases.Children())
        //    {
        //        Console.WriteLine("Release: {0}", release.GetValue("name"));
        //        Console.WriteLine("Id: {0}", release.GetValue("id"));
        //        Console.WriteLine("Published: {0}", DateTime.Parse(release.GetValue("published_at").ToString()));
        //        Console.WriteLine();
        //    }

        //    Console.ReadLine();
        //}
        static void Main(string[] args)
        {

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var username = "cvsym_51";
            var passcode = "73c27d0553d7606cfd9edc02ffb28eeeb8b1ee8df2712f5cbc081635b3f8719f";
            string url = $"https://uat.citivelocity.com/analytics/eppublic/charting/data/v1?login={username}&passcode={passcode}";
            var payload = new
            {
                startDate = "20170108",
                endDate = "20170114",
                tags = new List<string>
                {
                    "FX.SPOT.USD.CAD.CITI"
                }
            };

            var jsonString = JsonConvert.SerializeObject(payload);
            //HttpWebRequest(url, jsonString);
            RestSharp(url, jsonString);
            Console.WriteLine("\n\n\n");
            Console.ReadLine();
        }
        public class SpotData
        {
            [JsonProperty("x")]
            public List<int> Years { get; set; }
            [JsonProperty("c")]
            public List<decimal> Rates { get; set; }
            [JsonProperty("type")]
            public string SpotType { get; set; }
            //[JsonIgnore]
            //public Tuple<string, decimal> SpotRates {
            //    get { return Tuple.Create(null, 0.0); } 
            //}
        }

        static void RestSharp(string url, string jsonString)
        {
            var client = new RestClient(url);
            var request = new RestRequest();
            request.Method = Method.POST;
            request.Parameters.Clear();
                        
            //           string tag = "COMMODITIES.SPOT.SPOT_GOLD";

            //construct content to send
            Console.WriteLine(jsonString);

            request.AddParameter("application/json",
                 jsonString,
                 ParameterType.RequestBody);


            client.ExecuteAsync(request, response =>
            {

                if (response != null && ((response.StatusCode == System.Net.HttpStatusCode.OK) &&
                                         (response.ResponseStatus == ResponseStatus.Completed)))
                {

                    Console.WriteLine(response.Content);

                    var body = JObject.Parse(response.Content);
                    var result = JsonConvert.DeserializeObject<IDictionary<string, SpotData>>(body["body"].ToString());

                    foreach (var item in result)
                    {
                        Console.WriteLine($"Item: {item.Key}");

                        var spotData = item.Value;

                        Console.WriteLine($"c={string.Join(", ", spotData.Years)}");
                        Console.WriteLine($"x={string.Join(", ", spotData.Rates)}");
                        Console.WriteLine($"type={spotData.SpotType}");
                        Console.WriteLine();
                        Console.WriteLine();
                    }

                    Console.WriteLine("\n" + response.StatusCode);
                    Console.WriteLine("\n" + response.StatusDescription);
                    Console.WriteLine("\n" + response.ResponseStatus);

                }
                else if (response != null)
                {
                    Console.WriteLine(response.StatusCode);
                    Console.WriteLine(response.StatusDescription);
                    Console.WriteLine(response.ResponseStatus);
                    Console.WriteLine(response.ErrorMessage);
                    Console.WriteLine(response.ErrorException);

                    MessageBox.Show(string.Format
                    ("Status code is {0} ({1}); response status is {2}",
                        response.StatusCode/*, response.StatusDescription, response.ResponseStatus*/));
                }
            });

        }

        static void HttpWebRequest(string url, string jsonContent)
        {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";

                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                Byte[] byteArray = encoding.GetBytes(jsonContent);

                request.ContentLength = byteArray.Length;
                request.ContentType = @"application/json";

                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
                long length = 0;
                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        length = response.ContentLength;
                    }
                }
                catch (WebException ex)
                {
                    // Log exception and throw as for GET example above
                }
        }
        static void RestSharp1()
        {
            var client = new RestClient("https://sandbox.tradier.com/v1/markets");
            //Console.WriteLine("client: " + client);
            var request = new RestRequest("quotes", Method.GET);
            //Console.WriteLine("request: " + request);
            request.AddParameter("symbol", "AAPL");
            request.AddHeader("Authorization", "Bearer XCx8tMPkiTHHDC5GOAyTfjsORuuF");

            client.ExecuteAsync(request, response => {

                if (response != null && ((response.StatusCode == System.Net.HttpStatusCode.OK) &&
                                         (response.ResponseStatus == ResponseStatus.Completed)))
                {
                    string content = "[" + response.Content + "]";
                    var quotes = GetQuoteHistory(content);
                    foreach (JObject quote1 in quotes.Children())
                    {
                        foreach (JToken quote in quote1.Children())
                        {
                            Console.WriteLine("symbol:" + quote.Value<string>("symbol") + " open:" + quote.Value<string>("open") + " high:" + quote.Value<string>("high"));
                        }
                         
     /*           "high": 62.84,
                "low": 62.125,
                "close": 62.58,
                "volume": 20694101*/

                    }


                   // Console.WriteLine(response.Content);
                }
                else if (response != null)
                {
                    MessageBox.Show(string.Format
                    ("Status code is {0} ({1}); response status is {2}",
                        response.StatusCode, response.StatusDescription, response.ResponseStatus));
                }
            });
        }

        private static JArray GetQuoteHistory(string content)
        {
            return JArray.Parse(content);
        }

        public static JToken GetReleases(IRequestHandler requestHandler)
        {
            return requestHandler.GetReleases(RequestConstants.Url);
        }
    }
}
