﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RESTfulAPIConsume
{
    public class ChartingData
    {
        [JsonProperty("frequency")]
        public string frequency;
        [JsonProperty("body")]
        public IDictionary<string, SpotData> data;
        [JsonProperty("status")]
        public string status;
    }
    public class SpotData
    {
        [JsonProperty("x")]
        public List<string> Dates { get; set; }
        //        public List<string> Dates { get; set; }
        [JsonProperty("o")]
        public List<decimal> OpenRates { get; set; }
        [JsonProperty("h")]
        public List<decimal> HighRates { get; set; }
        [JsonProperty("l")]
        public List<decimal> LowRates { get; set; }
        [JsonProperty("c")]
        public List<decimal> CloseRates { get; set; }
        [JsonProperty("type")]
        public string SpotType { get; set; }

        public DateTime DateTimeVal(string DateStr) => new DateTime(long.Parse(DateStr));
        public string Year(string DateStr) => DateStr.Substring(0, 4);
        public string Week(string DateStr) => DateStr.Substring(4, 2);
        //[JsonIgnore]
        //public Tuple<string, decimal> SpotRates {
        //    get { return Tuple.Create(null, 0.0); } 
        //}
    }


    public class JsonParser
    {
        public static ChartingData parseJsonFromFile(string fileName)
        {
            string strData = File.ReadAllText(fileName);

            return parseJsonChartingData(strData);
        }

        public static ChartingData parseJsonChartingData(string strData)
        {
            var jsonData = JObject.Parse(strData);
            var result = JsonConvert.DeserializeObject<ChartingData>(strData);

            return result;
        }

    }
}
